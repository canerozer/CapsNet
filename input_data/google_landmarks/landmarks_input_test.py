import os

import tensorflow as tf

import landmarks_input_record as lm

class LandmarksInputTest(tf.test.TestCase):

    def _record(self, colors, label, key):
        """Creates the input record"""
        image_size = 224 * 224
        record = bytes(
            # bytearray([colors[0]] * image_size + [colors[1]] * image_size +
            #           [colors[2]] * image_size + [label] + [key]))
            bytearray([colors * image_size] + [label] + [key]))
        expected = [[colors] * 224 * 224]
        return record, expected

    def testRead(self):
      """Tests if the records are read in the expected order and value."""
      labels = [0, 1450, 9000, 14950]
      colors = [[0, 0, 0], [255, 255, 255], [1, 100, 253], [154, 153, 152]]
      keys = ["helloworldhellow", "helloworldhellow", "helloworldhellow",
             "helloworldhellow", "helloworldhellow"]
      records = []
      expecteds = []
      for i in range(4):
        record, expected = self._record(colors[i], labels[i], keys[i])
        records.append(record)
        expecteds.append(expected)
      filename = os.path.join(self.get_temp_dir(), "google_landmark_test")
      open(filename, "wb").write(b"".join(records))

      with self.test_session() as sess:
        q = tf.FIFOQueue(100, [tf.string], shapes=())
        q.enqueue([filename]).run()
        q.close().run()
        image_tensor, label_tensor, key_tensor = lm._read_input(q)

        for i in range(3):
          image, label, key = sess.run([image_tensor, label_tensor, key_tensor])
          self.assertEqual(labels[i], label)
          self.assertAllEqual(expecteds[i], image)
          self.assertEqual(keys[i], key)

        with self.assertRaises(tf.errors.OutOfRangeError):
          sess.run(image_tensor)

    def testBatchedOuput(self):
      """Tests if the final output of batching works properly."""
      record, _ = self._record([255, 0, 128], 5, "abadigubabadigub")
      batch_size = 10
      expected_labels = [5 for _ in range(batch_size)]
      data_dir = self.get_temp_dir()
      filename = os.path.join(data_dir, "test_batch.bin")
      open(filename, "wb").write(b"".join([record]))
      features = lm.inputs("test", data_dir, batch_size)

      with self.test_session() as sess:
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)
        labels = sess.run(features["recons_label"])
        self.assertAllEqual(expected_labels, labels)
        coord.request_stop()
        for thread in threads:
          thread.join()


if __name__ == "__main__":
  tf.test.main()
