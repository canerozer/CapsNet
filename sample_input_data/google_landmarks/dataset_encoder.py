# -*- coding: utf-8 -*-

# !/usr/bin/python

# Note: requires the tqdm package (pip install tqdm)

# Note to Kagglers: This script will not run directly in Kaggle kernels. You
# need to download it and run it on your local machine.

# Downloads images from the Google Landmarks dataset using multiple threads.
# Images that already exist will not be downloaded again, so the script can
# resume a partially completed download. All images will be saved in the JPG
# format with 90% compression quality.

import sys, os, multiprocessing, csv
from urllib import request, error
from PIL import Image
from io import BytesIO
from itertools import repeat
from collections import defaultdict
import tqdm
import time
import pickle
import numpy as np
import skimage.io as io


def parse_data(data_file):
    csvfile = open(data_file, 'r')
    csvreader = csv.reader(csvfile)
    key_url_list = [line[:3] for line in csvreader]
    indexes = [str(i) for i in range(len(key_url_list))]
    return key_url_list[1:], indexes  # Chop off header

def append_log_file(error_log_dir, error_message):
    print(error_message)
    with open(error_log_dir, "a+") as f:
        f.write(error_message+"\n")

def write_where_did_i_stopped(index, objective):
    with open("I_stopped_here"+("train" if objective else "test")+".txt", "w") as f:
        f.write(str(index))

def read_where_did_i_stopped(objective):
    with open("I_stopped_here"+("train" if objective else "test")+".txt", "r") as f:
        last_index = f.read()
    return last_index

def download_image(key_url_w_index):
    key_url, index, objective = key_url_w_index

    size = (256, 256)
    log_dir = "error_log_"+("train" if objective else "test")+".txt"
    out_dir = sys.argv[2]
    try:
        key, url, label = key_url
    except:
        key, url = key_url
    index = int(index)
    filename = os.path.join(out_dir, '{}.jpg'.format(key))

    if os.path.exists(filename):
    #    write_where_did_i_stopped(index, objective)
        print('Image {} already exists. Skipping download.'.format(filename))
        return 0

    try:
        response = request.urlopen(url)
        image_data = response.read()
    except:
        error = ('Warning: Could not download image {} from {}'.format(key, url))
        append_log_file(log_dir, error)
        return 1

    try:
        pil_image = Image.open(BytesIO(image_data))
    except:
        error = ('Warning: Failed to parse image {}'.format(key))
        append_log_file(log_dir, error)
        return 1

    try:
        pil_image_rgb = pil_image.convert('RGB')
    except:
        error = ('Warning: Failed to convert image {} to RGB'.format(key))
        append_log_file(log_dir, error)
        return 1

    try:
        pil_resized_image_rgb = pil_image_rgb.resize(size)
    except:
        error = ('Warning: Failed to resize the image {} to {}'.format(key, size))
        append_log_file(log_dir, error)
        return 1

    try:
        buffer = BytesIO()
        pil_resized_image_rgb.save(buffer, format='JPEG', quality=90)
        image = buffer.getvalue()

    except:
        error = ('Unable to write the compressed image to a buffer.')
        append_log_file(log_dir, error)
        return 1


    if objective:
        with open(os.path.join(out_dir, "train_batch_{}.bin".format(index)), "a+b") as f:
            data = (image, label, key)
            try:
                previous_content = list(pickle.load(f, pickle.HIGHEST_PROTOCOL))
            except:
                previous_content = []

            #previous_content.append(data)
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    elif not objective:
        with open(os.path.join(out_dir, "test_batch_{}.bin".format(index)), "a+b") as f:
            data = (image, key)
            try:
                previous_content = list(pickle.load(f, pickle.HIGHEST_PROTOCOL))
            except:
                previous_content = []
            #previous_content.append(data)
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

    #try:
    #    pil_resized_image_rgb.save(filename, format='JPEG', quality=90)
    #except:
    #    error = ('Warning: Failed to save image {}'.format(filename))
    #    append_log_file(log_dir, error)
    #    return 1

    #write_where_did_i_stopped(index, objective)
    return 0


def loader(bin_len, batch_size, train=True):
    if len(sys.argv) != 3:
        print('Syntax: {} <data_file.csv> <output_dir/>'.format(sys.argv[0]))
        sys.exit(0)
    (data_file, out_dir) = sys.argv[1:]


    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    key_url_list, indexes = parse_data(data_file)

    labels_dict = defaultdict(list)

    for key, url, label in key_url_list:
        labels_dict[label].append((key, url))

    num_labels = len(labels_dict.keys())
 
    binary_num = 0
    while(True):
        rand_classes = np.random.randint(low=0, high=num_labels, size=bin_len*batch_size)
        rand_classes = [list(labels_dict.keys())[i] for i in rand_classes]

        indices = (np.random.randint(0, len(labels_dict[i])) for i in rand_classes)
        key_url_list = [((labels_dict[i][j][0], labels_dict[i][j][1], i), binary_num, train)
                                        for i, j in zip(rand_classes, indices)]

        try:
            index = int(read_where_did_i_stopped(train))
        except:
            index = 0

        pool = multiprocessing.Pool(processes=16)  # Num of CPUs
        failures = sum(tqdm.tqdm(pool.imap_unordered(download_image, key_url_list), total=len(key_url_list)))
        print('Total number of download failures:', failures)
        pool.close()
        pool.terminate()
        binary_num += 1
        yield True

# arg1 : data_file.csv
# arg2 : output_dir
if __name__ == '__main__':
    loader_gen = loader(150, 128)
    for _ in range(95):
        next(loader_gen)
