import numpy as np
import pickle
import os
import io
from PIL import Image
from array import array
import time

time_start = time.time()
with open("binary_train/train_batch_0.bin", "rb") as f:
    batch_size = 12500
    images = np.zeros((batch_size, 3, 256, 256))
    labels = np.zeros((batch_size, ))
    d = 0
    try:
        while True:
            content = pickle.load(f)

            byteimage, label, filename = content[0][0], content[0][1], content[0][2]

            images[d] = np.array(Image.open(io.BytesIO(byteimage))).reshape(3, 256, 256)
            labels[d] = label
            if d%1000 == 0:
                print("{}".format(d))
            d+=1
    except:
        pass

print(time.time() - time_start)
